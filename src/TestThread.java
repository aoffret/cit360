

import java.util.Random;

public class TestThread implements Runnable {

    private int number1;
    private String name;
    private int number2;
    private int sleep;
    private int rand;


    public TestThread(String name, int number1, int number2, int sleep){
        this.name = name;
        this.number1 = number1;
        this.number2 = number2;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(10);
    }
    //have the project run equations and display the final object.
    @Override
    public void run() {
        System.out.println(name + " is " + number1 + " + " + number2 + ".");
        for (int i = 1; i < rand; i++){
            System.out.println(name + "is processing + " + i + " out of " + rand);
            try {
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        int total = number1 + number2;
        System.out.println("\nThe total of " + name + " is: " + total);
    }

}

