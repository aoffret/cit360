package com.company;

import java.util.List;

public class Grades {
    private String courseName;
    private String teacher;
    private int grade;

    public void setCourseName(String courseName){
        this.courseName = courseName;
    }

    public void setTeacher(String teacher){
        this.teacher = teacher;
    }

    public void setGrade(int grade){
        this.grade = grade;
    }

    public String toString(){
        String s =("Course: " + courseName + "\n Teacher: " + teacher + "\n Grade: " + grade);
        return s;
    }


}
