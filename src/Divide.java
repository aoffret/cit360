

import java.net.*;
import java.io.*;
import java.util.*;

public class Divide {

    public static String getHttpContent(String string){
        try {
            URL url = new URL(string);  //sets up connection
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            //data reader and connects with stream
            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));

            StringBuilder stringBuilder = new StringBuilder(); //makes string

            String line = null;     //Reader takes line in stream --> readLine reads it
            while ((line = reader.readLine()) != null) {  //Reads each stream line, and checks for last one by seeing if null
                stringBuilder.append(line + "\n"); //adds line and makes next line
            }

            return stringBuilder.toString(); //returns item

        } catch (IOException e){
            System.err.println(e.toString()); //prints error to string
        }

        return "Error"; //if error in url
    }

    public static Map getHttpHeaders(String string){

        try{
            URL url = new URL(string);  //makes URL
            HttpURLConnection http = (HttpURLConnection) url.openConnection(); //sets up connection

            return http.getHeaderFields(); //returns headers
        } catch (IOException e){
            System.err.println(e.toString());
        }
        return null; //error return
    }

    public static void main(String[] args) {
        System.out.println(Divide.getHttpContent("http://www.google.com"));
        Map<Integer, List<String>> m = Divide.getHttpHeaders("http://www.google.com"); //connects map to headers

        for (Map.Entry<Integer, List<String>> entry : m.entrySet()){
            System.out.println("Key= " + entry.getKey() + "Value= " + entry.getValue()); //prints headers
        }
    }
}
