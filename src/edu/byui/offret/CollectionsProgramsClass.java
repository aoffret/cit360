package edu.byui.offret;

import java.util.*;

class CollectionsProgramClass {
   public static void main(String[] args) {
      //Determines what type of collection program user would like to test.
      Scanner programDesired = new Scanner(System.in);
      System.out.println("What collection program do you want to test?");
      System.out.println("Options include: Queue, TreeSet, List, or HashedSet");
      String option = programDesired.nextLine();

      //Sorts out appropriate programs
      if (option.equals("Queue")) {
         /*Queue Program: Create a program that takes a list of clients,
            and demonstrates who is ready to be served on a FIFO basis. */

         Scanner name = new Scanner(System.in); //object that takes in user input (names)
         Queue<String> names = new LinkedList<String>(); // creating list of names

         //Gathering names of customers + identifying purpose of program.
         System.out.println("The Queue is for determining customers in a salon and determining who should be served first.");
         System.out.println("What's the first name of the customer? (Hit Enter after putting in each name. Max 5.");

         //gets names of customers and inserts them into the queue
         for (int cust = 0; cust < 5; cust++) {
            String custName = name.nextLine();
            names.add(custName);
         }

         //Displays first customer that needs to be served
         String custServed = names.remove();
         System.out.println("The next customer to be served is " + custServed + ".");

      } else if (option.equals("TreeSet")){
         //TreeSet: Used to collect list of library books and put them in alphabetical order.
         //Creates the treeSet
         TreeSet<String> listOfBooks=new TreeSet<String>();

         //States purpose of TreeSet:
         System.out.println("This TreeSet takes different titles of books and automatically puts them in alphabetial order.");

         //Adding Books out of order into the TreeSet
         listOfBooks.add("Harry Potter: The Sorcerer's Stone");
         listOfBooks.add("Absalom, Absalom!");
         listOfBooks.add("East of Eden");
         listOfBooks.add("Pride and Prejudice");
         listOfBooks.add("Ready Player One");
         listOfBooks.add("Twilight");

         //Prints the Books in alphabetical order
         System.out.println("The books in sorted / alphabetical order are: ");
         System.out.println(listOfBooks);

      } else if(option.equals("List")){
         //List of grades of students
         //Creates the list of grades
         List<Integer> listOfGrades =new ArrayList<Integer>();

         //adding grades
         listOfGrades.add(89);
         listOfGrades.add(72);
         listOfGrades.add(99);
         listOfGrades.add(43);

         //printing out grades
         System.out.println("The list of grades from students:" +listOfGrades);

         //Updating grade 4 (or 3 since it starts at 0) due to make up work
         listOfGrades.set(3, 80);
         System.out.println("Student 4 turned in new assignments. Here is the updated list of grades: " + listOfGrades);

         //Determining which student the user wants to look up
         Scanner num = new Scanner(System.in); //object that takes in user input (names)
         System.out.println("Which students grade would you like to look up? (1-4)");
         Integer studentNum = num.nextInt(); //studentNum = Student's index - 1

         //prints out students grade
         System.out.println("Student " + (studentNum) + " has the grade of " + listOfGrades.get(studentNum-1) + ".");
      } else if (option.equals("HashedSet")){
         //HashedSet to see to determine if the user has already packed an item.

         //hashSet containing the items that you've packed
         HashSet<String> packedItems = new HashSet<String>();

         //Scanner for user input
         Scanner scan = new Scanner(System.in);

         //list of items already packed
         packedItems.add("Water Bottle");
         packedItems.add("Rope");
         packedItems.add("Snacks");
         packedItems.add("Backpack");
         packedItems.add("Tent");
         packedItems.add("Carabiner");
         packedItems.add("Boots");
         packedItems.add("Flashlight");

         //prints out items in HashedSet
         System.out.println("Items packed include: " + packedItems);

         //scanner for user input

         System.out.println("The HashedSet is used to list items already packed for a camping trip.");
         System.out.println("Did you pack this item? (enter item you want to check):");
         String check = scan.nextLine();
         if (packedItems.contains(check) == true){
            System.out.println("Yes, you have packed this item!");
         } else {
            System.out.println("No, you have not packed this item yet.");
         }
      }
      else {
         System.out.println("Please enter an appropriate response.");
      }

   }
}







