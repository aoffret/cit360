package com.company;

import com.sun.net.httpserver.HttpServer;

import java.net.*;
import java.io.*;
import java.util.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.*;
//import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class httpServer {

    public static void main(String[] args) {
        HttpServer server;

        try{
            server = HttpServer.create(new InetSocketAddress("localhost", 8080), 0);
            System.out.println(server);
        } catch (IOException e) {
            System.out.println("Can't start listener");
            return;
        }

        System.out.println("Listener is working");

        HttpContext defaultContext = (HttpContext) server.createContext("/");

        defaultContext.setHandler( new jsonHandler() );

        server.start();


    }
}


/*  int port = 8080;
          //  server.bind(new InetSocketAddress(InetAddress.getLocalHost()), 0);
            server.createContext("/httpServer.java", new HttpHandler() {
                @Override
                public void handle(HttpExchange exchange) throws IOException {
                    return exchange.getRequestURI().toString();
                }
                public void handleResponse (HttpExchange exchange, String requestValue) throws IOException{
                    OutputStream outputSteam = exchange.getResponseBody();
                    exchange.sendResponseHeaders(200, json.length);
                }

            });
            ExecutorService executor = Executors.newFixedThreadPool(10);
            server.setExecutor(executor); //processes threads of task going on


           */
