package com.company;

import java.net.*;
import java.io.*;
import java.util.*;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

public class JsonClient {
    public static String gradesToJson (Grades grade){

        ObjectMapper mapper = new ObjectMapper();
        String string = "";

        try {
            string = mapper.writeValueAsString(grade);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }
        return string;
    }

    public static Grades jsonToGrade(String s) {

        ObjectMapper mapper = new ObjectMapper();
        Grades grade = null;

        try {
            grade = mapper.readValue(s, Grades.class);

        } catch (JsonMappingException e) {
            System.err.println(e.toString()); //resolve issue
        } catch (JsonProcessingException e) {
            System.err.println(e.toString()); //resolve issue
        }
        return grade;
    }

    public static Grades createGradeObject(Grades grade, String teacher, int numGrade, String courseName) {
        grade.setTeacher(teacher);
        grade.setGrade(numGrade);
        grade.setCourseName(courseName);
        return grade;
    }


    public static void main(String[] args) {

        Grades grade = new Grades();
        //how to get input information to JSON? Placeholder information
        // grade =createGradeObject(grade, "teacher",0, "holder");
        //System.out.println(grade.toString());

        grade.setTeacher("teacher");
        grade.setGrade(67);
        grade.setCourseName("cit360");

        String json = JsonClient.gradesToJson(grade);
        System.out.println(json);

       // Grades grade2 = JsonClient.jsonToGrade(json);
       // System.out.println(grade2);

    }
}

//Maybe craft