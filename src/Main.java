import java.util.*;

public class Main {

    private static Object ArithmeticException;

    public static float divideNum(int num1, int num2) throws Throwable {
        //Divides num1 and num2
        try {
            float result = (float) num1 / (float) num2;
            return result;
        } catch (ArithmeticException e) {
            System.out.println("The denominator can not be 0. Please try again.");
            return 0;
        }

    }
    //main process that processes the input and rings up the method
    public static void main(String[] args) throws Throwable {
        //Gathers num1 and num2
        int i = 0;
        float result = 0;
        int numerator = 0;
        int denominator = 0;

        //cycles through and accepts inputs from user. It gives the user 10 chances to give valid input.
        while ( i < 10) {

            Scanner input = new Scanner(System.in);
            try {

                //Takes user input
                System.out.println("Please input a numerator: ");
                numerator = input.nextInt();

                System.out.println("Please input a denominator: ");
                denominator = input.nextInt();

                //Determines if the denominator is 0 or not, and accesses the divide method.
                if (denominator == 0) {
                    System.out.println("The denominator cannot be a 0. Please try again.");
                } else {
                    result = divideNum(numerator, denominator);
                    i = 10;
                }

                //What happens when you put a letter instead of a number.
            } catch (InputMismatchException e) {
                System.out.println("Please enter whole numbers instead of letters.");

                //displays the fraction result if the denominator isn't 0.
            } finally {
                if (denominator != 0) {
                    System.out.println("The result of your fraction is: " + result);
                }
            }
        }
            i++;
    }
}

