package com.company;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;



public class LibraryBookTest {

    //test for assertSame
    @Test
    public void testGetName(){
        LibraryBook book = new LibraryBook();
       // String bookName = book.getBookName("Harry Potter");
        book.getBookName("Harry Potter");
        assertSame("Harry Potter", book.bookName);
    }
    //test for assertNull
    @Test
    public void testISBNNull(){
        LibraryBook book = new LibraryBook();
        book.getBookName(null);
        assertNull(book.bookName);
    }
    //test for assertNull
    @Test
    public void testNotNull(){
        LibraryBook book = new LibraryBook();
        book.getBookName("Harry Potter");
        assertNotNull(book.bookName);
    }

    @Test
    public void testEquals() {
        LibraryBook book = new LibraryBook();
        book.getBookName("Harry Potter");
        LibraryBook book1 = book;
        assertEquals(book, book1);
    }

    @Test
    public void testNotSame() {
        LibraryBook book = new LibraryBook();
        book.getBookName("Harry Potter");
        LibraryBook book1 = book;
        book1.getBookName("HP");
        boolean test = book1.bookName == book.bookName;
        System.out.println(test);
        //assertNotSame(book.bookName, book1.bookName);
    }

    @Test
    public void testFalse() {
        LibraryBook book = new LibraryBook();
        book.getBookName("Harry Potter");
        LibraryBook book1 = new LibraryBook();
        book1.getBookName("HP");
        assertFalse(book.bookName == book1.bookName);
    }
    @Test
    public void correctName() {
        LibraryBook book = new LibraryBook();
        book.getBookName("Harry Potter");
        assertTrue(book.bookName == "Harry Potter");
    }

    @Test
    public void printBookInfo() {
        LibraryBook book = new LibraryBook();
        String[] bookInfo = new String[2];

        book.getBookName("Harry Potter");
        book.getAuthor("JK Rowling");


        bookInfo[0] = book.bookName;
        bookInfo[1] = book.author;

        String[] testArray = {"Harry Potter", "JK Rowling"};
        System.out.println(bookInfo + " " + testArray);

        assertArrayEquals(testArray, bookInfo);

    }

    @Test
    public void testAssert(){
        LibraryBook book = new LibraryBook();
        book.getBookName("Harry Potter");
        String bookName = book.bookName;
        assertThat(bookName, is("Harry Potter"));
    }


}