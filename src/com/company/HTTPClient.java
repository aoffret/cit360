package com.company;

import java.net.*;
import java.io.*;
import java.util.*;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.*;



//maybe request list of classes and grade within this using arrays and
 // maps maybe use localhost to look it up.
public class HTTPClient {

     public static String retrieveHTTPContent(String urlString) {
         String s = "";
         try {
             //System.out.println(urlString);
             URL url = new URL(urlString);
             HttpURLConnection connection = (HttpURLConnection) url.openConnection();
             BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

             StringBuilder stringBuild = new StringBuilder();

             String string = null;
             while ((string = reader.readLine()) != null) {
                 stringBuild.append(string + "\n");
             //    System.out.println("String is: " + string);
                 s = string;
             }
            // System.out.println(s);
            return s;
         } catch (IOException e) {
             System.err.println("Error");
             System.err.println(e.toString());
             return s;
         }
     }

    public static void main(String[] args) {


        String response = retrieveHTTPContent("http://localhost:8080/");
        System.out.println("It connected locally.");

        ObjectMapper mapper = new ObjectMapper();
        Grades grade = null;

        try {
            grade = mapper.readValue(response, Grades.class);

        } catch (JsonMappingException e) {
            System.err.println(e.toString()); //resolve issue
        } catch (JsonProcessingException e) {
            System.err.println(e.toString()); //resolve issue
            System.out.println("Unable to parse the string.");
        }

        System.out.println(grade);
    }



}
