/*package com.example.demo;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "servlet",urlPatterns={"/servlet"})
public class HelloServlet extends HttpServlet {
    private String message;


    //Uses to redirect you, won't work without this
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        String bookName = request.getParameter("bookName");
        String author = request.getParameter("author");
        String lastCheckedOut = request.getParameter("lastCheckedOut");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("Book name is: " + bookName + "\n");
        out.println("Author is: " + author + "\n");
        out.println("Last Checked Out: " + lastCheckedOut + "\n");
        out.println("</body></html>");
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("This resource is not available directly.");
    }


    public void destroy() {
    }
}

other notes: When running tomcat, will auto setup war and execute html code
Need index file
If have hard problem and can't submit --> mabye something wrong with the @WebServlet line
If nothing works, make sure see no errors in output
 */
