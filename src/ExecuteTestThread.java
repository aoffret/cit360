//package com.company;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteTestThread {
    public static final ExecutorService service = Executors.newFixedThreadPool(3);

    public static void main(String[] args) {

        TestThread t1 = new TestThread("Equation 1", 30, 65, 1000);
        TestThread t2 = new TestThread("Equation 2", 70, 84, 900);
        TestThread t3 = new TestThread("Equation 3", 95, 10, 500);
        TestThread t4 = new TestThread("Equation 4", 15, 90, 200);
        TestThread t5 = new TestThread("Equation 5", 14, 10 , 100);

        service.execute(t1);
        service.execute(t2);
        service.execute(t3);
        service.execute(t4);
        service.execute(t5);

        service.shutdown();
    }
}