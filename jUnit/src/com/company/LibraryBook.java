package com.company;

import java.lang.reflect.Array;
import java.util.List;

public class LibraryBook {
    public String bookName;
    public String author;
    public int isbn;
    public LibraryBook[] listofBooks = new LibraryBook[5];

    //assign values to project
    public String getBookName(String bookName){
        this.bookName = bookName;
        //throw exception if input is not a string
        return this.bookName;
    }

    public void getAuthor (String author){
        this.author = author;
    }

    public void getISBN(int isbn){
        this.isbn = isbn;
    }

    public void listOfBooks(LibraryBook book) {
        int i = 0;
        while (i < 5) {
            System.out.println(listofBooks[i]);
            if (listofBooks[i] == null) {
                listofBooks[i] = book;
            }
            i++;
        }
    }

    public void testPrint(String bookName, String author, int isbn){
        System.out.println("Name: " + bookName + " Author: " + " ISBN: " + isbn);
    }

}
